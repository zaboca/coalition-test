<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
    /**
     * Display input form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getForm()
    {
        // Check if file exists
        if (!file_exists(storage_path('data.json'))) {
            file_put_contents(storage_path('data.json'), json_encode([]));
        }

        // Get existing records
        $data = json_decode(file_get_contents(storage_path('data.json')), true);
        
        return view('pages.index', compact('data'));
    }

    public function postForm(Request $request)
    {
        $this->validate($request, [
            'product_name' => 'required|string',
            'product_stock' => 'required|integer',
            'product_price' => 'required|numeric'
        ]);
        
        $date = new \DateTime();
        
        $record = [
            'product_name' => $request->input('product_name'),
            'product_stock' => $request->input('product_stock'),
            'product_price' => $request->input('product_price'),
            'datetime_submitted' => $date->format('Y-m-d H:i:s')
        ];

        // Get existing records
        $data = json_decode(file_get_contents(storage_path('data.json')), true);

        // Append new record
        $data[] = $record;
        file_put_contents(storage_path('data.json'), json_encode($data));

        return json_encode(['lines' => $data]);
    }
    
    public function getData()
    {
        // Get existing records
        $data = json_decode(file_get_contents(storage_path('data.json')), true);
        return json_encode(['lines' => $data]);
    }
}
