<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'page.form', 'uses' => 'PagesController@getForm']);
Route::get('get', ['as' => 'get.data', 'uses' => 'PagesController@getData']);
Route::post('/', ['as' => 'page.submit', 'uses' => 'PagesController@postForm']);
