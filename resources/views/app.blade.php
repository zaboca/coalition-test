
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row">
            @yield('content')
        </div>
    </div>

<script src="{{ url('https://code.jquery.com/jquery-1.12.4.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var form, data, data1, block, i, row;

        $.get("get", function(data) {
            updateRecords(data);
        });

        $('#form').on('submit', function (e) {
            e.preventDefault();

            form = $(this);

            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function(data)
                {
                    updateRecords(data);
                }
            });
        });

        function updateRecords(data) {
            block = $('tbody');
            data1 = JSON.parse(data);
            row = '';
            console.log(data);
            for (i = 0; i < data1.lines.length; i++) {
                row += '<tr>';
                row += '<td>'+data1.lines[i].product_name+'</td>';
                row += '<td>'+data1.lines[i].product_stock+'</td>';
                row += '<td>'+data1.lines[i].product_price+'</td>';
                row += '<td>'+data1.lines[i].datetime_submitted+'</td>';
                row += '<td>'+data1.lines[i].product_stock*data1.lines[i].product_price+'</td>';
                row += '</tr>'
            }
            block.empty().append(row);
        }
    });
</script>
</body>
</html>