@extends('app')

@section('content')
    <div class="col-md-8 col-md-offset-2" style="margin-top:100px;">
        <div class="panel panel-default">
            <div class="panel-body">
                <form id="form" class="form-horizontal" action="{{ route('page.submit') }}" method="post" accept-charset="utf-8" style="margin-bottom: 25px;">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="product_name" class="col-md-4">Product Name</label>
                        <div class="col-md-8">
                            <input type="text" name="product_name" id="product_name" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="product_stock" class="col-md-4">Qty. in stock</label>
                        <div class="col-md-4">
                            <input type="number" step="1" name="product_stock" id="product_stock" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="product_price" class="col-md-4">Price per Item</label>
                        <div class="col-md-4">
                            <input type="number" name="product_price" id="product_price" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Submit Form
                            </button>
                        </div>
                    </div>
                </form>
                <table class="table table-striped" id="records">
                    <thead>
                    <tr>
                        <th>Product name</th>
                        <th>Quantity in Stock</th>
                        <th>Price per Item</th>
                        <th>Datetime submitted</th>
                        <th>Total Value Number</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
@endsection